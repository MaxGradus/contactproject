<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.javacourse.contact.domain.Contact"%>
<%@page import="java.util.List"%>

<html>
<head>
    <title>Contact List</title>
</head>
<body>
  Contact List<br/>

  <table border="1">
      <tr>
          <td>ID</td>
          <td>SurName</td>
          <td>GivenName</td>
          <td>Date</td>
          <td>e-mail</td>
          <td>Phone</td>
      </tr>
<%
    List<Contact> list = (List<Contact>)request
            .getAttribute("ContactList");
    for(Contact c : list) {
%>
    <tr>
        <td>
            <a href="getContact?contactId=<%=c.getContactId()%>">
                <%=c.getContactId()%>
            </a>
        </td>
        <td><%=c.getSurName()%></td>
        <td><%=c.getGivenName()%></td>
        <td><%=c.getDateOfBirth()%></td>
        <td><%=c.getEmail()%></td>
        <td><%=c.getPhone()%></td>
    </tr>
<%
    }
%>
  </table>

</body>
</html>
