package com.javacourse.contact.web;

import com.javacourse.contact.business.ContactManager;
import com.javacourse.contact.domain.Contact;
import com.javacourse.contact.exception.ContactBusinessException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class FindContactServlet extends HttpServlet
{
    private ContactManager manager;

    @Override
    public void init() throws ServletException {
        super.init();
        manager = ContactManager.getInstance();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Contact> model = manager.findContacts(null);
            request.setAttribute("ContactList", model);
            getServletContext()
                    .getRequestDispatcher("/contactList.jsp")
                    .forward(request, response);
        } catch (ContactBusinessException e) {
            e.printStackTrace(System.out);
        }
    }
}
