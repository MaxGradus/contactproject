package com.javacourse.contact.exception;

public class ContactDAOException extends ContactException
{
    public ContactDAOException(String message) {
        super(message);
    }

    public ContactDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
