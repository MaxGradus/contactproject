package com.javacourse.contact.exception;

public class ContactBusinessException extends ContactException
{
    public ContactBusinessException(String message) {
        super(message);
    }

    public ContactBusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
