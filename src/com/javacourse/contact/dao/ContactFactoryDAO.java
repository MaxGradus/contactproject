package com.javacourse.contact.dao;



public class ContactFactoryDAO {
    private static final String CONFIG_NAME = "contactProject.properties";
    private static final String DAO_CLASS = "contact.dao";

    private static ContactDAO instance = null;

    private ContactFactoryDAO() {
    }

    public synchronized static ContactDAO getDao() {
        if (instance == null) {
            try {
                instance = new ContactDBDAO();
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
        return instance;
    }
}
