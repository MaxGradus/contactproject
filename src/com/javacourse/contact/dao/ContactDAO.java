package com.javacourse.contact.dao;

import com.javacourse.contact.domain.Contact;
import com.javacourse.contact.exception.ContactDAOException;
import com.javacourse.contact.filter.ContactFilter;

import java.util.List;

public interface ContactDAO {
    public List<Contact> findContacts(ContactFilter filter) throws ContactDAOException;

    public Contact getContact(Long contactId) throws ContactDAOException;

    public Long addContact(Contact contact) throws ContactDAOException;

    public void deleteContact(Long contactId) throws ContactDAOException;

    public void updateContact(Contact contact) throws ContactDAOException;
}
