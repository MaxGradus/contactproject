package com.javacourse.contact.dao;

import com.javacourse.contact.domain.Contact;
import com.javacourse.contact.exception.ContactDAOException;
import com.javacourse.contact.exception.ContactException;
import com.javacourse.contact.filter.ContactFilter;

import java.util.*;

public class ContactFakeDAO implements ContactDAO
{
    private List<Contact> contactList = new ArrayList<Contact>();

    @Override
    public List<Contact> findContacts(ContactFilter filter) throws ContactDAOException {
        if (filter == null) {
            throw new ContactDAOException("Filter is NULL");
        }
        return contactList;
    }

    @Override
    public Contact getContact(Long contactId) throws ContactDAOException {
        for (Iterator<Contact> cnt = contactList.iterator();
             cnt.hasNext(); ) {
            Contact contact = cnt.next();
            if (contact.getContactId().equals(contactId)) {
                return contact;
            }
        }
        for (Contact cnt : contactList) {
            if (cnt.getContactId().equals(contactId)) {
                return cnt;
            }
        }
        throw new ContactDAOException("No contact for ID:" + contactId);
    }

    @Override
    public Long addContact(Contact contact) throws ContactDAOException {
        Long id = new Random().nextLong();
        contact.setContactId(id);
        contactList.add(contact);
        return id;
    }

    @Override
    public void deleteContact(Long contactId) throws ContactDAOException {
        Contact contact = null;
        for (Contact cnt : contactList) {
            if (cnt.getContactId().equals(contactId)) {
                contact = cnt;
                break;
            }
        }
        contactList.remove(contact);
    }

    @Override
    public void updateContact(Contact contact) throws ContactDAOException {

    }
}
