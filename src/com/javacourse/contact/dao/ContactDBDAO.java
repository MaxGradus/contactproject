package com.javacourse.contact.dao;

import com.javacourse.contact.domain.Contact;
import com.javacourse.contact.exception.ContactDAOException;
import com.javacourse.contact.filter.ContactFilter;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.PropertyResourceBundle;

public class ContactDBDAO implements ContactDAO {
    public static final String CLASS_NAME = "class";
    public static final String DB_URL = "url";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";

    private String className;
    private String url;
    private String login;
    private String password;
    private Connection connection;

    public ContactDBDAO() throws Exception {
        init();
    }

    public void init() throws Exception {
        PropertyResourceBundle prb = (PropertyResourceBundle) PropertyResourceBundle.getBundle("com/javacourse/contact/dao/dbdao");
        className = prb.getString(CLASS_NAME);
        url = prb.getString(DB_URL);
        login = prb.getString(LOGIN);
        password = prb.getString(PASSWORD);
        connection = createConnection();
    }

    private Connection createConnection() throws Exception {
        Class.forName(className);
        return DriverManager.getConnection(url, login, password);
    }

    public Connection getConnection() {
        return connection;
    }

    @Override
    public List<Contact> findContacts(ContactFilter filter) throws ContactDAOException {
        List<Contact> result = new LinkedList<Contact>();
        try {
            PreparedStatement stmt = getConnection().prepareStatement(
                    "SELECT * FROM cm_contact ORDER BY sur_name");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Contact c = new Contact();
                c.setContactId(rs.getLong("contact_id"));
                c.setSurName(rs.getString("sur_name"));
                c.setGivenName(rs.getString("given_name"));
                c.setDateOfBirth(rs.getDate("date_of_birth"));
                c.setEmail(rs.getString("email"));
                c.setPhone(rs.getString("phone"));
                result.add(c);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result;
    }

    @Override
    public Contact getContact(Long contactId) throws ContactDAOException {
        return null;
    }

    @Override
    public Long addContact(Contact contact) throws ContactDAOException {
        Long contactId = 0L;
        try {
            PreparedStatement stmt = getConnection().prepareStatement(
                    "INSERT INTO cm_contact " + "(sur_name, given_name, date_of_birth, email, phone) " +
                            "VALUES (?, ?, ?, ?, ?)",
                    new String[]{"contact_id"}
            );
            stmt.setString(1, contact.getSurName());
            stmt.setString(2, contact.getGivenName());
            long t = contact.getDateOfBirth().getTime();
            stmt.setDate(3, new java.sql.Date(t));
            stmt.setString(4, contact.getEmail());
            stmt.setString(5, contact.getPhone());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                contactId = rs.getLong(1);
            }
            rs.close();

            stmt.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return contactId;
    }

    @Override
    public void deleteContact(Long contactId) throws ContactDAOException {
        String deleteSQL = "DELETE  FROM contact_db.cm_contact WHERE contact_id =" + contactId;
        try {
            Statement stmnt = getConnection().createStatement();
            stmnt.executeUpdate(deleteSQL);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateContact(Contact contact) throws ContactDAOException {
        Long id = contact.getContactId();
        String updateQuery = "UPDATE contact_db.cm_contact SET sur_name = ?, given_name = ?," +
                "email = ?, phone = ? WHERE contact_id =" + id;
        try {
            PreparedStatement stmnt = getConnection().prepareStatement(updateQuery);
            stmnt.setString(1, contact.getSurName());
            stmnt.setString(2, contact.getGivenName());
            stmnt.setString(3, contact.getEmail());
            stmnt.setString(4, contact.getPhone());
            stmnt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
