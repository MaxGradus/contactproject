package com.javacourse.contact.business;

import com.javacourse.contact.dao.ContactDAO;
import com.javacourse.contact.dao.ContactFactoryDAO;
import com.javacourse.contact.domain.Contact;
import com.javacourse.contact.exception.ContactBusinessException;
import com.javacourse.contact.exception.ContactDAOException;
import com.javacourse.contact.exception.ContactException;
import com.javacourse.contact.filter.ContactFilter;

import java.util.List;

public class ContactManager {
    private static ContactManager instance;

    private ContactDAO dao;

    public ContactManager() {
        setDao(ContactFactoryDAO.getDao());
    }

    public static synchronized ContactManager getInstance() {
        if(instance == null) {
            instance = new ContactManager();
        }
        return instance;
    }

    public void setDao(ContactDAO dao) {
        this.dao = dao;
    }

    public List<Contact> findContacts(ContactFilter filter) throws ContactBusinessException {
        List<Contact> result = null;
        try {
            result = dao.findContacts(filter);
        } catch (ContactDAOException ex) {
            System.out.println("ContactDAOException");
            ContactBusinessException cbe =
                    new ContactBusinessException(ex.getMessage());
            throw cbe;
        } finally {
        }
        return result;
    }

    public Contact getContact(Long contactId) throws ContactBusinessException {
        try {
            Contact c = dao.getContact(contactId);
            return c;
        } catch (ContactDAOException e) {
            e.printStackTrace();
        }
        throw new ContactBusinessException("No contact for ID:" + contactId);
    }

    public Long addContact(Contact contact) throws ContactBusinessException {
        try {
            return dao.addContact(contact);
        } catch (ContactDAOException e) {
            e.printStackTrace();
            throw new ContactBusinessException(e.getMessage(), e);
        }
    }

    public void deleteContact(Long contactId) {
        try {
            dao.deleteContact(contactId);
        } catch (ContactDAOException e) {
            e.printStackTrace();
        }
    }

    public void updateContact(Contact contact)
            throws ContactBusinessException {
        try {
            dao.updateContact(contact);
        } catch (ContactDAOException e) {
            e.printStackTrace();
            throw new ContactBusinessException(e.getMessage(), e);
        }
    }
}
