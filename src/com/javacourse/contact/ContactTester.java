package com.javacourse.contact;

import com.javacourse.contact.business.ContactManager;
import com.javacourse.contact.dao.ContactDAO;
import com.javacourse.contact.dao.ContactFactoryDAO;
import com.javacourse.contact.domain.Contact;
import com.javacourse.contact.exception.ContactBusinessException;
import com.javacourse.contact.filter.ContactFilter;

import java.util.Date;
import java.util.List;

public class ContactTester
{
    public static void main(String[] args) {
        ContactManager cm = new ContactManager();
        ContactDAO dao = ContactFactoryDAO.getDao();
        cm.setDao(dao);

        try {
            Contact test = createContact();
            /**ADD CONTACT*/
            Long contactId = cm.addContact(test);
            System.out.println("Contact is added:" + contactId);


            /**PRINT CONTACT LIST*/
            List<Contact> res = cm.findContacts(new ContactFilter());
            System.out.println("Contacts ListSize: " +  res.size());
            System.out.println("Contacts' list is got");
            for(Contact cnt : res) {
                System.out.println(cnt.getContactId());
            }

//            Contact cntTest = cm.getContact(res.get(0).getContactId());
//            System.out.println("Contact is got:" + cntTest.getContactId());

            /**UPDATE CONTACT*/
//            res.get(0).setEmail("newmail@pisem.net");
//            cm.updateContact(res.get(0));
//            System.out.println("Contact is updated");

            /**DELETE CONTACT*/
//            cm.deleteContact(res.get(0).getContactId());
//            System.out.println("Contact is deleted");

//            List<Contact> res2 = cm.findContacts(new ContactFilter());
//            System.out.println("Contacts list is got 2");
//            for(Contact cnt : res2) {
//                System.out.println(cnt.getContactId());
//            }


        } catch (ContactBusinessException ex) {
            ex.printStackTrace(System.out);
        }
    }

    public static Contact createContact() {
        Contact test = new Contact();
        test.setSurName("Larkin");
        test.setGivenName("Ivan");
        test.setDateOfBirth(new Date());
        test.setEmail("ivan@pisem.net");
        test.setPhone("+7-812-555-7055");
        return test;
    }
}
